import javax.swing.JOptionPane;

public class Menu {
	public static void main(String[] deerhunter) {
		System.out.println("Ruleta Rusa");
		
		int estado = Integer.parseInt(JOptionPane.showInputDialog("Que Accion a Seguir?"+"\n"+"1-Comenzar Juego"+"\n"+"2-Salir"));
		while (estado == 1) {
			Juego DeNiro = new Juego();
			int a = Integer.parseInt(JOptionPane.showInputDialog("Ingrese Dificultad"+"\n"+"1-Facil"+"\n"+"2-Medio"+"\n"+"3-Dificil"));
			DeNiro.setDificultad(a);
			DeNiro.cargarBala();
			System.out.println("Bala Cargada");
			DeNiro.girar();
			System.out.println("Revolver Revuelto");
			
			while(DeNiro.isTerminado()==false) {
				int b = Integer.parseInt(JOptionPane.showInputDialog("Donde Disparar"+"\n"+"1-Cabeza"+"\n"+"2-Aire"));
				DeNiro.dondeDisparar(b);
				DeNiro.disparo();
				DeNiro.haTerminado();
				if (DeNiro.isDisparado()==true && DeNiro.isDispCabeza()==true) {
					System.out.println("Has Muerto :(");
				}else if (DeNiro.isDisparado()==false & DeNiro.isDispCabeza()==true) {
					System.out.println("El Juego Continua");
				}else if(DeNiro.isDisparado()==true & DeNiro.isDispCabeza()==false) {
					System.out.println("El Arma Disparo, ganaste el juego!!");
				}else if (DeNiro.isDisparado()==false & DeNiro.isDispCabeza()==false) {
					System.out.println("El Arma no Disparo, mala adivinanza, perdiste...");
				}
			}
			
			estado = Integer.parseInt(JOptionPane.showInputDialog("Que Accion a Seguir?"+"\n"+"1-Comenzar Juego"+"\n"+"2-Salir"));
		}
		
	}

}
