import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestJuego {
	private Juego jjj;
	@BeforeEach
	void SetUp() {
		this.jjj = new Juego();
	}
	
	@Test
	void test() {
		jjj.setDificultad(1);
		jjj.cargarBala();
		jjj.girar();
		while(jjj.isDisparado()== false) {
			assertFalse(jjj.isTerminado());
			jjj.dondeDisparar(1);
			jjj.disparo();
		}
		jjj.haTerminado();
		assertTrue(jjj.isDisparado());
		assertTrue(jjj.isDispCabeza());
		assertFalse(jjj.isGanado());
		assertTrue(jjj.isTerminado());
		assertEquals(jjj.getBala(), jjj.getDisparo());
	}
	
	@Test
	void test2(){
		jjj.setDificultad(2);
		jjj.cargarBala();
		jjj.girar();
		while(jjj.isTerminado()==false) {
			jjj.dondeDisparar(2); 
			jjj.disparo();
			jjj.haTerminado();
		}
		jjj.haTerminado();
		if(jjj.isGanado()==false) {
			assertFalse(jjj.isDispCabeza());
			assertFalse(jjj.isGanado());
			assertTrue(jjj.isTerminado());
			assertNotEquals(jjj.getBala(), jjj.getDisparo());
		}else if (jjj.isGanado()==true) {
			assertFalse(jjj.isDispCabeza());
			assertTrue(jjj.isGanado());
			assertTrue(jjj.isTerminado());
			assertEquals(jjj.getBala(), jjj.getDisparo());
			
		}
	}
	
	@Test
	void test3(){
		jjj.setDificultad(3);
		jjj.cargarBala();
		jjj.girar();
		while(jjj.isTerminado()==false) {
			assertFalse(jjj.isTerminado());
			jjj.setDisparo(jjj.getBala()-1);
			jjj.dondeDisparar(2); 
			jjj.disparo();
			jjj.haTerminado();
		}
		jjj.haTerminado();
		assertFalse(jjj.isDispCabeza());
		assertTrue(jjj.isGanado());
		assertTrue(jjj.isTerminado());
		assertEquals(jjj.getBala(), jjj.getDisparo());
	}
	
	@Test
	void test4() {
		jjj.setDificultad(6);
		jjj.cargarBala();
		jjj.girar();
		while(jjj.isDisparado()== false) {
			jjj.setBala(2);
			jjj.setDisparo(1);
			assertFalse(jjj.isTerminado());
			jjj.setDispCabeza(false);
			jjj.disparo();
		}
		jjj.haTerminado();
		assertFalse(jjj.isDispCabeza());
		assertTrue(jjj.isGanado());
		assertTrue(jjj.isTerminado());
		assertEquals(jjj.getBala(), jjj.getDisparo());
	}

}
