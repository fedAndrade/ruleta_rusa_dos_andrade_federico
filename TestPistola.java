import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestPistola {
	private Pistola ppp;
	
	@BeforeEach
	void SetUp() {
		this.ppp = new Pistola();
		ppp.setCartuchos(6);
		ppp.cargarBala();
		ppp.girar();
	}
	
	@Test
	void test(){
		while (ppp.isDisparado() == false) {
			assertNotEquals(ppp.getBala(), ppp.getDisparo());
			assertFalse(ppp.isDisparado());
			ppp.disparo();
		}
		assertEquals(ppp.getBala(), ppp.getDisparo());
		assertTrue(ppp.isDisparado());
	}
	
	@Test
	void test2() {
		assertEquals(ppp.getCartuchos(), 6);
		ppp.setDisparo(7);
		ppp.setBala(3);
		while (ppp.isDisparado() == false) {
			ppp.disparo();
		}
	}
}
