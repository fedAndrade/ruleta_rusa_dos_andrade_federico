import javax.swing.JOptionPane;

public class Juego extends Pistola{
	private int dificultad;
	private boolean dispCabeza;
	private boolean ganado;
	private boolean terminado;
	
	public Juego() {
		super.setDisparado(false);
		terminado=false;
	}
	
	
	public void dondeDisparar(int dispi) {
		if (dispi == 1) 
			dispCabeza = true;
		else 
			dispCabeza = false;
	}

	public void haTerminado() {
		if (super.isDisparado()==true && dispCabeza==false) {
			ganado=true;
			terminado=true;
		} else if (super.isDisparado()==false && dispCabeza==false) {
			ganado=false;
			terminado=true;
		}else if (super.isDisparado()==true && dispCabeza==true) {
			ganado=false;
			terminado=true;
		}else if (super.isDisparado()==false && dispCabeza==true)  {
			terminado=false;			
		}
	}


	
	public void setDificultad(int dificultad) {
		this.dificultad = dificultad;
		if (dificultad > 0 & dificultad < 4)
			super.setCartuchos(dificultad*2+4);
		else
			super.setCartuchos(8);
	}

	public boolean isDispCabeza() {
		return dispCabeza;
	}

	public void setDispCabeza(boolean dispCabeza) {
		this.dispCabeza = dispCabeza;
	}
	public boolean isGanado() {
		return ganado;
	}
	public boolean isTerminado() {
		return terminado;
	}
}
