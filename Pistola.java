import java.util.Random;

public class Pistola {
	private int cartuchos;
	private boolean disparado;
	private int bala;
	private int disparo;
    Random rand = new Random(); 
	
	public Pistola() {
		setDisparado(false);
	}
	
	public void cargarBala() {
		bala = rand.nextInt(cartuchos-1)+1;
	}
	
	public void girar() {
		disparo = rand.nextInt(cartuchos-1)+1;
	}
	
	public void disparo() {
		disparo = disparo + 1;
		if (disparo == cartuchos+1) {
			disparo = 1;
		}
		if (disparo == bala){
			setDisparado(true);
		}
	}
	

	public int getCartuchos() {
		return cartuchos;
	}
	public void setCartuchos(int cartuchos) {
		this.cartuchos = cartuchos;
	}
	public int getBala() {
		return bala;
	}
	public void setBala(int bala) {
		this.bala = bala;
	}
	public void setDisparo(int disparo) {
		this.disparo = disparo;
	}
	public int getDisparo() {
		return disparo;
	}
	public boolean isDisparado() {
		return disparado;
	}
	public void setDisparado(boolean disparado) {
		this.disparado = disparado;
	}

}
